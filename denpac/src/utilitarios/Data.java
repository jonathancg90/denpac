/*
 * CLase contenedora de variables globales del sistema
 */
package utilitarios;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

/**
 *
 * @author Jonathan
 */
public class Data {
    
    public String G_USER[] = new String[2];
    public String G_STATES[] = new String[2];
    public String G_TYPE[] = new String[35];
    private boolean G_ESTATES_BOL[] = new boolean[2];
    private DefaultComboBoxModel MChoice;
    
    /**
    * Inicializacion de datos.
    */
    public Data() {
        setEstados();
        setUser();
        setTypo();
    }
    
    /**
    * Opciones de reportes disponibles.
    */
    private void setEstados() {
        G_STATES[0] = "Inactivo";
        G_STATES[1] = "Activo";
        
        G_ESTATES_BOL[0] = false;
        G_ESTATES_BOL[1] = true;
    }
    public void loadState(JComboBox cboestate, boolean state) {
            MChoice = new DefaultComboBoxModel();
            if(state == false){
                MChoice.addElement(G_STATES[0]);
                MChoice.addElement(G_STATES[1]);
            }
            else{
                MChoice.addElement(G_STATES[1]);
                MChoice.addElement(G_STATES[0]);
            }
                
            cboestate.setModel(MChoice); 
    }
    public boolean getEstado(String estado){
        for(int i=0;i<=G_STATES.length;i++) {
            if(G_STATES[i].equals(estado)) {
                return G_ESTATES_BOL[i];
            }
        }
        return false;
    }
    /**
    * Opciones de reportes disponibles.
    */
    private void setUser() {
        G_USER[0] = "MAWESDI";
        G_USER[1] = "QUIRQUINCHO1";
    }
    private void setTypo() {
        G_TYPE[0] = "Examen Dental - Modelos";
        G_TYPE[1] = "Profilaxis - Destartraje";
        G_TYPE[2] = "Exodoncias";
        G_TYPE[3] = "Curetaje de Encia";
        G_TYPE[4] = "Endodoncias";
        G_TYPE[5] = "Resinas Simple - Luz Halógena";
        G_TYPE[6] = "Resinas Compuestas - Luz Halógena";
        G_TYPE[7] = "Amalgama Simple";
        G_TYPE[8] = "Amalgama Compuesta";
        G_TYPE[9] = "Endodoncia Anterior";
        G_TYPE[10] = "Endodoncia Posterior";
        G_TYPE[11] = "Perno Muñón";
        G_TYPE[12] = "Encrustación";
        G_TYPE[13] = "Coronas de Porcelana";
        G_TYPE[14] = "Coronas de Isosit";
        G_TYPE[15] = "Coronas de Oro";
        G_TYPE[16] = "Corona Venner";
        G_TYPE[17] = "Prótesis Parcial - Metálica Removible";
        G_TYPE[18] = "Prótesis Completa Superior";
        G_TYPE[19] = "Prótesis Completa Inferior";
        G_TYPE[20] = "Prótesis Fija - Porcelana";
        G_TYPE[21] = "Prótesis Fija de Oro";
        G_TYPE[22] = "Prótesis Fija de Isosit";
        G_TYPE[23] = "Rebace de Prótesis";
        G_TYPE[24] = "Pulpotomias";
        G_TYPE[25] = "Balance Oclusal";
        G_TYPE[26] = "Topicación de Fluor";
        G_TYPE[27] = "Aparato Ortodóntico - Removible";
        G_TYPE[28] = "Ortodoncia Fija";
        G_TYPE[29] = "Mantenedor Espacio";
        G_TYPE[30] = "Examen Radiográfico Periapical";
        G_TYPE[31] = "Examen Radiográfico Oclusal";
        G_TYPE[32] = "Blanqueamiento Dental";
        G_TYPE[33] = "Bacteriostaticos - Sellantes";
        G_TYPE[34] = "Otros";
    }
    public int getType(String type){
        for(int i=0;i<=G_TYPE.length;i++) {
            if(G_TYPE[i].equals(type)) {
                return i;
            }
        }
        return 0;
    }
    public void loadType(JComboBox cbotype, String type) {
            MChoice = new DefaultComboBoxModel();
            for(int i=0;i<G_TYPE.length;i++) {
                if(G_TYPE[i].equals(type)) {
                    MChoice.addElement(G_TYPE[i]);
                }
            }
            for(int i=0;i<G_TYPE.length;i++) {
                if(!G_TYPE[i].equals(type)) {
                    MChoice.addElement(G_TYPE[i]);
                }
            } 
            cbotype.setModel(MChoice); 
    }
}
