
package utilitarios;

import java.util.Calendar;


public class Helpers {
    
    
     /**
     * Obtener la fecha actual.
     */
    public String getDateNow(){
        String date;
        
        Calendar c = Calendar.getInstance();
        int mesact = c.get(Calendar.MONTH);
        if(mesact == 0){
            mesact = mesact +1;
        }
        
        String dia = Integer.toString(c.get(Calendar.DATE));
        String mes = Integer.toString(mesact);
        String annio = Integer.toString(c.get(Calendar.YEAR));
        date = annio+"-"+mes+"-"+dia;
        //date = dia+"-"+mes+"-"+annio;
                
        return date;
    }
}
