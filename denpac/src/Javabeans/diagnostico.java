
package Javabeans;


public class diagnostico {
    
    private int iddiag;
    private int idpac;
    private int tipo;
    private String descripcion;
    private double costo;
    private String created;

    public diagnostico(int iddiag, int idpac, int tipo, String descripcion, double costo, String created) {
        this.iddiag = iddiag;
        this.idpac = idpac;
        this.tipo = tipo;
        this.descripcion = descripcion.toUpperCase();
        this.costo = costo;
        this.created = created;
    }

    public diagnostico() {
    }

    public void setIddiag(int iddiag) {
        this.iddiag = iddiag;
    }

    public void setIdpac(int idpac) {
        this.idpac = idpac;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion.toUpperCase();
    }

    public void setCosto(double costo) {
        this.costo = costo;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public int getIddiag() {
        return iddiag;
    }

    public int getIdpac() {
        return idpac;
    }

    public int getTipo() {
        return tipo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public double getCosto() {
        return costo;
    }

    public String getCreated() {
        return created;
    }
    
    
}
