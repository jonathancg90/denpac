
package Javabeans;


public class paciente {

    private int idpac;
    private String nombre;
    private String apellido;
    private String telefono;
    private String direccion;
    private boolean estado;
    private String created;

    public paciente() {
    }

    public paciente(int idpac, String nombre, String apellido, String telefono, String direccion, boolean estado, String created) {
        this.idpac = idpac;
        this.nombre = nombre.toUpperCase();
        this.apellido = apellido.toUpperCase();
        this.telefono = telefono;
        this.direccion = direccion.toUpperCase();
        this.estado = estado;
        this.created = created;
    }

    public void setIdpac(int idpac) {
        this.idpac = idpac;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre.toUpperCase();
    }

    public void setApellido(String apellido) {
        this.apellido = apellido.toUpperCase();
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion.toUpperCase();
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public int getIdpac() {
        return idpac;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public boolean isEstado() {
        return estado;
    }

    public String getCreated() {
        return created;
    }

    
}
