
package Gui;
import Dao.pacientesDAO;
import javax.swing.JOptionPane;
import utilitarios.Data;
import Javabeans.paciente;
import javax.swing.table.DefaultTableModel;
import Dao.diagnosticoDAO;
import java.awt.Image;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.filechooser.FileNameExtensionFilter;
import utilitarios.ImagePreviewPanel;

public class frmpaciente extends javax.swing.JFrame {

    private pacientesDAO pac;
    private diagnosticoDAO diag;
    private Data dt;
    private paciente obj_pac;
    private JFileChooser fileChooser;
    private FileNameExtensionFilter fl;
    
    public frmpaciente() {
        initComponents();
        cargaform();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnagre = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        btnelim = new javax.swing.JButton();
        btnrepo = new javax.swing.JButton();
        btnsalir = new javax.swing.JButton();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel9 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        txtnom = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        txtape = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        txttel = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        txtdirec = new javax.swing.JTextField();
        jPanel11 = new javax.swing.JPanel();
        Cboestado = new javax.swing.JComboBox();
        lblid = new javax.swing.JLabel();
        jButton4 = new javax.swing.JButton();
        lblfotopac = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        JtblDiag = new javax.swing.JTable();
        jButton2 = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        lblcantdiag = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        lbliddiag = new javax.swing.JLabel();
        jButton5 = new javax.swing.JButton();
        lblfotdiag = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        Jtblpago = new javax.swing.JTable();
        jButton7 = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        lblsum = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        Txtbuscar = new javax.swing.JTextField();
        btnbus = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        Jtblpaciente = new javax.swing.JTable();
        lblcant = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnagre.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/add_patient_icon_es.png"))); // NOI18N
        btnagre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnagreActionPerformed(evt);
            }
        });
        getContentPane().add(btnagre, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 610, 110, 70));

        jLabel1.setText("<html> \t<h1>Centro Odontológico \"San Juan Bautista\"</h1> </html>");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 0, -1, 40));

        btnelim.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/eliminar.jpeg"))); // NOI18N
        btnelim.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnelimActionPerformed(evt);
            }
        });
        getContentPane().add(btnelim, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 610, 96, 67));

        btnrepo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/reporte.jpeg"))); // NOI18N
        btnrepo.setText("Reporte Total");
        btnrepo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnrepoActionPerformed(evt);
            }
        });
        getContentPane().add(btnrepo, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 610, -1, 67));

        btnsalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cerrar_sesion.jpg"))); // NOI18N
        btnsalir.setText("Cerrar Sesión");
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });
        getContentPane().add(btnsalir, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 610, -1, 67));

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Nombre"));

        txtnom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnomActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtnom, javax.swing.GroupLayout.DEFAULT_SIZE, 284, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(txtnom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Apellido"));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtape)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(txtape, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Teléfono"));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txttel)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(txttel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Dirección"));

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtdirec, javax.swing.GroupLayout.DEFAULT_SIZE, 284, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(txtdirec, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(26, Short.MAX_VALUE))
        );

        jPanel11.setBorder(javax.swing.BorderFactory.createTitledBorder("Estado"));

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(Cboestado, 0, 133, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Cboestado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/boton_subir.png"))); // NOI18N
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        lblfotopac.setBorder(javax.swing.BorderFactory.createTitledBorder("Foto"));

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addGap(63, 63, 63)
                        .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(61, 61, 61))
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblid)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel9Layout.createSequentialGroup()
                                .addComponent(lblfotopac, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(49, 49, 49))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                                .addComponent(jButton4)
                                .addGap(108, 108, 108))))))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addComponent(lblfotopac, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(28, 28, 28)
                .addComponent(lblid)
                .addGap(72, 72, 72))
        );

        jTabbedPane1.addTab("Datos personales", jPanel9);

        JtblDiag.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        JtblDiag.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                JtblDiagMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(JtblDiag);

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/icono-nuevo.gif"))); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel3.setText("Cantidad");

        lblcantdiag.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/icono_quitar.gif"))); // NOI18N
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/boton_subir.png"))); // NOI18N
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        lblfotdiag.setBorder(javax.swing.BorderFactory.createTitledBorder("Foto"));

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 340, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jLabel3)
                        .addGap(30, 30, 30)
                        .addComponent(lblcantdiag, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbliddiag))
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton5))
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGap(47, 47, 47)
                        .addComponent(lblfotdiag, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(22, Short.MAX_VALUE))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addComponent(lblfotdiag, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jButton5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButton3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblcantdiag, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbliddiag)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Diagnosticos", jPanel10);

        Jtblpago.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane3.setViewportView(Jtblpago);

        jButton7.setText("Ver");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        jLabel4.setText("Total");

        lblsum.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jButton7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel4)
                        .addGap(18, 18, 18)
                        .addComponent(lblsum, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 545, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(41, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap(12, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jButton7, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addComponent(lblsum, javax.swing.GroupLayout.DEFAULT_SIZE, 19, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 254, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32))
        );

        jTabbedPane1.addTab("Costos", jPanel6);

        getContentPane().add(jTabbedPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 40, 620, 360));

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("Buscar"));

        jLabel2.setText("Nombres");

        btnbus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/buscar.jpeg"))); // NOI18N
        btnbus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnbusActionPerformed(evt);
            }
        });

        Jtblpaciente.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        Jtblpaciente.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                JtblpacienteMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(Jtblpaciente);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 564, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(24, Short.MAX_VALUE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnbus, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblcant)
                        .addGap(72, 72, 72))))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnbus, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(Txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(lblcant, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 108, Short.MAX_VALUE)
                .addContainerGap())
        );

        getContentPane().add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 410, 610, 200));

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/boton actualizar.png"))); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 609, 80, 70));

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void cargaform(){
        dt =  new Data();
        lbliddiag.setVisible(false);
        dt.loadState(Cboestado, true);
    }
    
    private void cleanBox() {
       txtnom.setText("");
       txtape.setText("");
       txttel.setText("");
       txtdirec.setText("");
    }
    private void cargaDiag() {
        diag =  new diagnosticoDAO();
        diag.getTableAll(JtblDiag, lblcantdiag, lblid.getText());
    }
    private void cargaimg(String ruta, String id,JLabel lblfoto){

        File archivo = new  File(ruta+id+".jpg");
             if(archivo.exists()){
                 String archivoimg = ruta+id+".jpg";
                 ImageIcon imagenfoto = new ImageIcon(archivoimg);
                 
                 Image iamgendimen = imagenfoto.getImage();
                  Image newimg = iamgendimen.getScaledInstance(140,170,java.awt.Image.SCALE_SMOOTH);
                  ImageIcon newIcon = new ImageIcon(newimg);
                  lblfoto.setIcon(newIcon);
             }else{
                 System.out.println("no eres file");
                ImageIcon imagenFondo = new ImageIcon(getClass().getResource(ruta+"defecto.jpg"));
                Image iamgendimen = imagenFondo.getImage();
                       Image newimg = iamgendimen.getScaledInstance(140,170,java.awt.Image.SCALE_SMOOTH);
                       ImageIcon newIcon = new ImageIcon(newimg);
                 lblfoto.setIcon(newIcon);
             }
    }
    private void upPhoto(String ruta, JLabel lblfoto, JLabel lblidfot) {
      if(!"".equals(lblidfot.getText())){
            fileChooser= new JFileChooser();
            
            ImagePreviewPanel preview = new ImagePreviewPanel();
            
            fileChooser.setAccessory(preview);
            fileChooser.addPropertyChangeListener(preview);
            fl= new FileNameExtensionFilter("imagenes", "jpg", "png","jpeg");
            fileChooser.setAcceptAllFileFilterUsed(false);
            fileChooser.setFileFilter(fl);
            int imagenElegida= fileChooser.showDialog(this,"Subir foto");
            if(imagenElegida == JFileChooser.APPROVE_OPTION) 
                {
                    File img =  fileChooser.getSelectedFile().getAbsoluteFile();

                    File dir = new File (ruta);
                    if(!dir.exists()){
                       dir.mkdir();
                    }

                    File foto = new File (lblidfot.getText()+".jpg");

                    img.renameTo(foto);

                    boolean semovio = foto.renameTo(new File (dir,foto.getName()));
                    String file =ruta+lblidfot.getText()+".jpg";
                    ImageIcon foto2 = new ImageIcon(file);
                    Image iamgendimen = foto2.getImage();
                    Image newimg = iamgendimen.getScaledInstance(140,170,java.awt.Image.SCALE_SMOOTH);
                    ImageIcon newIcon = new ImageIcon(newimg);
                    lblfoto.setIcon(newIcon);
                    if (!semovio){
                    System.out.println("error");
                    }
                }
        } else {
            JOptionPane.showMessageDialog(null,"Seleccione un empleado");
        }
    }
    private void txtnomActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnomActionPerformed

    }//GEN-LAST:event_txtnomActionPerformed

    private void btnagreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnagreActionPerformed
       try {
           pac = new pacientesDAO();
           dt = new Data();

           String nombre = txtnom.getText();
           String apellido = txtape.getText();
           String telefono = txttel.getText();
           String direccion = txtdirec.getText();
           boolean estado = dt.getEstado(Cboestado.getSelectedItem().toString());
           int i = pac.save(nombre, apellido, telefono, direccion, estado);
           if(i==0) {
                JOptionPane.showMessageDialog(null,"No se pudo registrar al paciente");
           }
           else if (i>0){
                JOptionPane.showMessageDialog(null,"Paciente registrado");
                cleanBox();
                pac.getTableAll(Jtblpaciente, lblcant, Txtbuscar.getText());
           }
       } catch(Exception e){
           System.out.println("Evento registrar: "+e);
       }
       
    }//GEN-LAST:event_btnagreActionPerformed

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        frmlogin objlogin = new frmlogin();
        objlogin.show();
        this.dispose();
        this.dispose();
    }//GEN-LAST:event_btnsalirActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

     if(lblid.getText() != "") {
        int idpac = Integer.parseInt(lblid.getText());
        try {
              pac = new pacientesDAO();
              dt = new Data();

              String nombre = txtnom.getText();
              String apellido = txtape.getText();
              String telefono = txttel.getText();
              String direccion = txtdirec.getText();
              boolean estado = dt.getEstado(Cboestado.getSelectedItem().toString());
              int i = pac.update(idpac, nombre, apellido, telefono, direccion, estado);
              if(i==0) {
                   JOptionPane.showMessageDialog(null,"No se pudo actualizar al paciente");
              }
              else if (i>0){
                   JOptionPane.showMessageDialog(null,"Paciente actualziado");
                   cleanBox();
                   pac.getTableAll(Jtblpaciente, lblcant, Txtbuscar.getText());
              }
          } catch(Exception e){
              System.out.println("Evento actualizar: "+e);
          }
     } else {
         JOptionPane.showMessageDialog(null,"Seleccione un paciente");
     }
     
    }//GEN-LAST:event_jButton1ActionPerformed

    private void btnelimActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnelimActionPerformed
     if(lblid.getText() != "") {
        int idpac = Integer.parseInt(lblid.getText());
        try {
              pac = new pacientesDAO();
              int i = pac.delete(idpac);
              if(i==0) {
                   JOptionPane.showMessageDialog(null,"No se pudo eliminar al paciente");
              }
              else if (i>0){
                   JOptionPane.showMessageDialog(null,"Paciente eliminado");
                   cleanBox();
                   pac.getTableAll(Jtblpaciente, lblcant, Txtbuscar.getText());
              }
          } catch(Exception e){
              System.out.println("Evento eliminar: "+e);
          }
     } else {
         JOptionPane.showMessageDialog(null,"Seleccione un paciente");
     }
    }//GEN-LAST:event_btnelimActionPerformed

    private void btnbusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnbusActionPerformed
        pac = new pacientesDAO();
        String nombre = this.Txtbuscar.getText();
        pac.getTableAll(Jtblpaciente, lblcant, nombre);
    }//GEN-LAST:event_btnbusActionPerformed

    private void JtblpacienteMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_JtblpacienteMouseClicked

        int fsel;
        dt = new Data();
        fsel = this.Jtblpaciente.getSelectedRow();
            try {
                pac = new pacientesDAO();
                obj_pac = new paciente();
                DefaultTableModel m = new DefaultTableModel();
                m = (DefaultTableModel) this.Jtblpaciente.getModel();
                String idPac = String.valueOf(m.getValueAt(fsel, 0));
                //Asigando valores obtenidos
                lblid.setText(idPac);
                obj_pac = pac.getValues(Integer.parseInt(idPac));
                txtnom.setText(obj_pac.getNombre());
                txtape.setText(obj_pac.getApellido());
                txttel.setText(obj_pac.getTelefono());
                txtdirec.setText(obj_pac.getDireccion());
                dt.loadState(Cboestado, obj_pac.isEstado());
                cargaDiag();
                cargaimg("imagenes/pacientes/",idPac,lblfotopac);
                cargaimg("imagenes/diagnosticos/","defecto",lblfotdiag);
                }
            catch (Exception e) {
                System.out.println("Evento seleccion:"+ e);
            }
    }//GEN-LAST:event_JtblpacienteMouseClicked

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        if(lblid.getText() != "") {
            frmdiagnostico objkey = new frmdiagnostico();
            objkey.lblid.setText(lblid.getText());
            objkey.lblempleado.setText(txtnom.getText()+" "+txtape.getText());
            objkey.show();
        } else {
            JOptionPane.showMessageDialog(null,"Seleccione un paciente");
        }
        
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
     if(lbliddiag.getText() != "") {
        int idpac = Integer.parseInt(lblid.getText());
        try {
              diag = new diagnosticoDAO();
              int i = diag.delete(Integer.parseInt(lbliddiag.getText()), Integer.parseInt(lblid.getText()));
              if(i==0) {
                   JOptionPane.showMessageDialog(null,"No se pudo eliminar el diagnostico");
              }
              else if (i>0){
                   JOptionPane.showMessageDialog(null,"diagnostico eliminado");
                   cargaDiag();
              }
          } catch(Exception e){
              System.out.println("Evento eliminar diagnostico: "+e);
          }
     } else {
         JOptionPane.showMessageDialog(null,"Seleccione un diagnostico");
     }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void JtblDiagMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_JtblDiagMouseClicked
        int fsel;
        dt = new Data();
        fsel = this.JtblDiag.getSelectedRow();
            try {
                pac = new pacientesDAO();
                obj_pac = new paciente();
                DefaultTableModel m = new DefaultTableModel();
                m = (DefaultTableModel) this.JtblDiag.getModel();
                String iddiag = String.valueOf(m.getValueAt(fsel, 0));
                //Asigando valores obtenidos
                lbliddiag.setText(iddiag);
                System.out.println("este: "+iddiag);
                cargaimg("imagenes/diagnosticos/",iddiag,lblfotdiag);
                }
            catch (Exception e) {
                System.out.println("Evento seleccion:"+ e);
            }
    }//GEN-LAST:event_JtblDiagMouseClicked

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
    upPhoto("imagenes/pacientes/",lblfotopac,lblid);

    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
    upPhoto("imagenes/diagnosticos/",lblfotdiag,lbliddiag);
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        pac = new pacientesDAO();
        pac.getTableAllPagos(Jtblpago, lblsum);
    }//GEN-LAST:event_jButton7ActionPerformed

    private void btnrepoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnrepoActionPerformed
        pac = new pacientesDAO();
        if(lblid.getText() !=""){
            pac.getReport(Integer.parseInt(lblid.getText()));
        }else{
            JOptionPane.showMessageDialog(null,"diagnostico eliminado");
        }
  
    }//GEN-LAST:event_btnrepoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frmpaciente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frmpaciente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frmpaciente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frmpaciente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new frmpaciente().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox Cboestado;
    private javax.swing.JTable JtblDiag;
    private javax.swing.JTable Jtblpaciente;
    private javax.swing.JTable Jtblpago;
    private javax.swing.JTextField Txtbuscar;
    private javax.swing.JButton btnagre;
    private javax.swing.JButton btnbus;
    private javax.swing.JButton btnelim;
    private javax.swing.JButton btnrepo;
    private javax.swing.JButton btnsalir;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton7;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel lblcant;
    private javax.swing.JLabel lblcantdiag;
    private javax.swing.JLabel lblfotdiag;
    private javax.swing.JLabel lblfotopac;
    private javax.swing.JLabel lblid;
    private javax.swing.JLabel lbliddiag;
    private javax.swing.JLabel lblsum;
    private javax.swing.JTextField txtape;
    private javax.swing.JTextField txtdirec;
    private javax.swing.JTextField txtnom;
    private javax.swing.JTextField txttel;
    // End of variables declaration//GEN-END:variables
}
